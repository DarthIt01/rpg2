// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RPG2_GameDataTables_generated_h
#error "GameDataTables.generated.h already included, missing '#pragma once' in GameDataTables.h"
#endif
#define RPG2_GameDataTables_generated_h

#define New_folder___Copy_Source_RPG2_GameDataTables_h_44_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMissionStruct_Statics; \
	RPG2_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> RPG2_API UScriptStruct* StaticStruct<struct FMissionStruct>();

#define New_folder___Copy_Source_RPG2_GameDataTables_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FWeaponStruct_Statics; \
	RPG2_API static class UScriptStruct* StaticStruct(); \
	typedef FTableRowBase Super;


template<> RPG2_API UScriptStruct* StaticStruct<struct FWeaponStruct>();

#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnFetchAllTables) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnFetchAllTables(); \
		P_NATIVE_END; \
	}


#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnFetchAllTables) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnFetchAllTables(); \
		P_NATIVE_END; \
	}


#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGameDataTables(); \
	friend struct Z_Construct_UClass_AGameDataTables_Statics; \
public: \
	DECLARE_CLASS(AGameDataTables, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RPG2"), NO_API) \
	DECLARE_SERIALIZER(AGameDataTables)


#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_INCLASS \
private: \
	static void StaticRegisterNativesAGameDataTables(); \
	friend struct Z_Construct_UClass_AGameDataTables_Statics; \
public: \
	DECLARE_CLASS(AGameDataTables, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RPG2"), NO_API) \
	DECLARE_SERIALIZER(AGameDataTables)


#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGameDataTables(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGameDataTables) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGameDataTables); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGameDataTables); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGameDataTables(AGameDataTables&&); \
	NO_API AGameDataTables(const AGameDataTables&); \
public:


#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGameDataTables(AGameDataTables&&); \
	NO_API AGameDataTables(const AGameDataTables&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGameDataTables); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGameDataTables); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGameDataTables)


#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_PRIVATE_PROPERTY_OFFSET
#define New_folder___Copy_Source_RPG2_GameDataTables_h_63_PROLOG
#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	New_folder___Copy_Source_RPG2_GameDataTables_h_66_PRIVATE_PROPERTY_OFFSET \
	New_folder___Copy_Source_RPG2_GameDataTables_h_66_RPC_WRAPPERS \
	New_folder___Copy_Source_RPG2_GameDataTables_h_66_INCLASS \
	New_folder___Copy_Source_RPG2_GameDataTables_h_66_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define New_folder___Copy_Source_RPG2_GameDataTables_h_66_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	New_folder___Copy_Source_RPG2_GameDataTables_h_66_PRIVATE_PROPERTY_OFFSET \
	New_folder___Copy_Source_RPG2_GameDataTables_h_66_RPC_WRAPPERS_NO_PURE_DECLS \
	New_folder___Copy_Source_RPG2_GameDataTables_h_66_INCLASS_NO_PURE_DECLS \
	New_folder___Copy_Source_RPG2_GameDataTables_h_66_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RPG2_API UClass* StaticClass<class AGameDataTables>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID New_folder___Copy_Source_RPG2_GameDataTables_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
