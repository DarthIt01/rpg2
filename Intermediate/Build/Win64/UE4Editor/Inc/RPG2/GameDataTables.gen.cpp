// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RPG2/GameDataTables.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGameDataTables() {}
// Cross Module References
	RPG2_API UScriptStruct* Z_Construct_UScriptStruct_FMissionStruct();
	UPackage* Z_Construct_UPackage__Script_RPG2();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTableRowBase();
	RPG2_API UScriptStruct* Z_Construct_UScriptStruct_FWeaponStruct();
	RPG2_API UClass* Z_Construct_UClass_AGameDataTables_NoRegister();
	RPG2_API UClass* Z_Construct_UClass_AGameDataTables();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	RPG2_API UFunction* Z_Construct_UFunction_AGameDataTables_OnFetchAllTables();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
// End Cross Module References
class UScriptStruct* FMissionStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RPG2_API uint32 Get_Z_Construct_UScriptStruct_FMissionStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMissionStruct, Z_Construct_UPackage__Script_RPG2(), TEXT("MissionStruct"), sizeof(FMissionStruct), Get_Z_Construct_UScriptStruct_FMissionStruct_Hash());
	}
	return Singleton;
}
template<> RPG2_API UScriptStruct* StaticStruct<FMissionStruct>()
{
	return FMissionStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMissionStruct(FMissionStruct::StaticStruct, TEXT("/Script/RPG2"), TEXT("MissionStruct"), false, nullptr, nullptr);
static struct FScriptStruct_RPG2_StaticRegisterNativesFMissionStruct
{
	FScriptStruct_RPG2_StaticRegisterNativesFMissionStruct()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("MissionStruct")),new UScriptStruct::TCppStructOps<FMissionStruct>);
	}
} ScriptStruct_RPG2_StaticRegisterNativesFMissionStruct;
	struct Z_Construct_UScriptStruct_FMissionStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Collect_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Collect;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Kill_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Kill;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMissionStruct_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMissionStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMissionStruct>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Collect_MetaData[] = {
		{ "Category", "MissionStruct" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Collect = { "Collect", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMissionStruct, Collect), METADATA_PARAMS(Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Collect_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Collect_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Kill_MetaData[] = {
		{ "Category", "MissionStruct" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Kill = { "Kill", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMissionStruct, Kill), METADATA_PARAMS(Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Kill_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Kill_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMissionStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Collect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMissionStruct_Statics::NewProp_Kill,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMissionStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RPG2,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"MissionStruct",
		sizeof(FMissionStruct),
		alignof(FMissionStruct),
		Z_Construct_UScriptStruct_FMissionStruct_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FMissionStruct_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMissionStruct_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FMissionStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMissionStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMissionStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RPG2();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MissionStruct"), sizeof(FMissionStruct), Get_Z_Construct_UScriptStruct_FMissionStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMissionStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMissionStruct_Hash() { return 3969328825U; }
class UScriptStruct* FWeaponStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RPG2_API uint32 Get_Z_Construct_UScriptStruct_FWeaponStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWeaponStruct, Z_Construct_UPackage__Script_RPG2(), TEXT("WeaponStruct"), sizeof(FWeaponStruct), Get_Z_Construct_UScriptStruct_FWeaponStruct_Hash());
	}
	return Singleton;
}
template<> RPG2_API UScriptStruct* StaticStruct<FWeaponStruct>()
{
	return FWeaponStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWeaponStruct(FWeaponStruct::StaticStruct, TEXT("/Script/RPG2"), TEXT("WeaponStruct"), false, nullptr, nullptr);
static struct FScriptStruct_RPG2_StaticRegisterNativesFWeaponStruct
{
	FScriptStruct_RPG2_StaticRegisterNativesFWeaponStruct()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("WeaponStruct")),new UScriptStruct::TCppStructOps<FWeaponStruct>);
	}
} ScriptStruct_RPG2_StaticRegisterNativesFWeaponStruct;
	struct Z_Construct_UScriptStruct_FWeaponStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CooldownTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CooldownTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Damage_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Damage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Icon_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Icon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponStruct_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWeaponStruct>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_CooldownTime_MetaData[] = {
		{ "Category", "WeaponStruct" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_CooldownTime = { "CooldownTime", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponStruct, CooldownTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_CooldownTime_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_CooldownTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Damage_MetaData[] = {
		{ "Category", "WeaponStruct" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Damage = { "Damage", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponStruct, Damage), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Damage_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Damage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "WeaponStruct" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponStruct, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_DisplayName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Icon_MetaData[] = {
		{ "Category", "WeaponStruct" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Icon = { "Icon", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWeaponStruct, Icon), METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Icon_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Icon_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWeaponStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_CooldownTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Damage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWeaponStruct_Statics::NewProp_Icon,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWeaponStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_RPG2,
		Z_Construct_UScriptStruct_FTableRowBase,
		&NewStructOps,
		"WeaponStruct",
		sizeof(FWeaponStruct),
		alignof(FWeaponStruct),
		Z_Construct_UScriptStruct_FWeaponStruct_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponStruct_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWeaponStruct_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FWeaponStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWeaponStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWeaponStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_RPG2();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WeaponStruct"), sizeof(FWeaponStruct), Get_Z_Construct_UScriptStruct_FWeaponStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWeaponStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWeaponStruct_Hash() { return 3388333813U; }
	void AGameDataTables::StaticRegisterNativesAGameDataTables()
	{
		UClass* Class = AGameDataTables::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnFetchAllTables", &AGameDataTables::execOnFetchAllTables },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AGameDataTables_OnFetchAllTables_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGameDataTables_OnFetchAllTables_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game DataTables" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGameDataTables_OnFetchAllTables_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGameDataTables, nullptr, "OnFetchAllTables", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGameDataTables_OnFetchAllTables_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGameDataTables_OnFetchAllTables_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGameDataTables_OnFetchAllTables()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGameDataTables_OnFetchAllTables_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AGameDataTables_NoRegister()
	{
		return AGameDataTables::StaticClass();
	}
	struct Z_Construct_UClass_AGameDataTables_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MissionsTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MissionsTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponsTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WeaponsTable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGameDataTables_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_RPG2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AGameDataTables_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AGameDataTables_OnFetchAllTables, "OnFetchAllTables" }, // 1560704537
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGameDataTables_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GameDataTables.h" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGameDataTables_Statics::NewProp_MissionsTable_MetaData[] = {
		{ "Category", "Game DataTables" },
		{ "ModuleRelativePath", "GameDataTables.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGameDataTables_Statics::NewProp_MissionsTable = { "MissionsTable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGameDataTables, MissionsTable), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGameDataTables_Statics::NewProp_MissionsTable_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGameDataTables_Statics::NewProp_MissionsTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGameDataTables_Statics::NewProp_WeaponsTable_MetaData[] = {
		{ "Category", "Game DataTables" },
		{ "Comment", "//I used edit anywhere, to be able to assign it in the details panel\n" },
		{ "ModuleRelativePath", "GameDataTables.h" },
		{ "ToolTip", "I used edit anywhere, to be able to assign it in the details panel" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGameDataTables_Statics::NewProp_WeaponsTable = { "WeaponsTable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGameDataTables, WeaponsTable), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGameDataTables_Statics::NewProp_WeaponsTable_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGameDataTables_Statics::NewProp_WeaponsTable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGameDataTables_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGameDataTables_Statics::NewProp_MissionsTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGameDataTables_Statics::NewProp_WeaponsTable,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGameDataTables_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGameDataTables>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGameDataTables_Statics::ClassParams = {
		&AGameDataTables::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AGameDataTables_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_AGameDataTables_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AGameDataTables_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AGameDataTables_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGameDataTables()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGameDataTables_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGameDataTables, 3970039310);
	template<> RPG2_API UClass* StaticClass<AGameDataTables>()
	{
		return AGameDataTables::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGameDataTables(Z_Construct_UClass_AGameDataTables, &AGameDataTables::StaticClass, TEXT("/Script/RPG2"), TEXT("AGameDataTables"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGameDataTables);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
