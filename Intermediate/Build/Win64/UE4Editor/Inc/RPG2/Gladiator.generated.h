// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RPG2_Gladiator_generated_h
#error "Gladiator.generated.h already included, missing '#pragma once' in Gladiator.h"
#endif
#define RPG2_Gladiator_generated_h

#define New_folder___Copy_Source_RPG2_Gladiator_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnChangeWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnChangeWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnAttack) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnAttack(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveRight(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveForward) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveForward(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnPostAttack) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPostAttack(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnGetHealthAmount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->OnGetHealthAmount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnChangeHealthByAmount) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_usedAmount); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnChangeHealthByAmount(Z_Param_usedAmount); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnSetPlayerController) \
	{ \
		P_GET_UBOOL(Z_Param_status); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnSetPlayerController(Z_Param_status); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetIsStillAlive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetIsStillAlive(); \
		P_NATIVE_END; \
	}


#define New_folder___Copy_Source_RPG2_Gladiator_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnChangeWeapon) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnChangeWeapon(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnAttack) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnAttack(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveRight(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execMoveForward) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MoveForward(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnPostAttack) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPostAttack(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnGetHealthAmount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->OnGetHealthAmount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnChangeHealthByAmount) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_usedAmount); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnChangeHealthByAmount(Z_Param_usedAmount); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnSetPlayerController) \
	{ \
		P_GET_UBOOL(Z_Param_status); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnSetPlayerController(Z_Param_status); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetIsStillAlive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetIsStillAlive(); \
		P_NATIVE_END; \
	}


#define New_folder___Copy_Source_RPG2_Gladiator_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGladiator(); \
	friend struct Z_Construct_UClass_AGladiator_Statics; \
public: \
	DECLARE_CLASS(AGladiator, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RPG2"), NO_API) \
	DECLARE_SERIALIZER(AGladiator)


#define New_folder___Copy_Source_RPG2_Gladiator_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAGladiator(); \
	friend struct Z_Construct_UClass_AGladiator_Statics; \
public: \
	DECLARE_CLASS(AGladiator, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RPG2"), NO_API) \
	DECLARE_SERIALIZER(AGladiator)


#define New_folder___Copy_Source_RPG2_Gladiator_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGladiator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGladiator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGladiator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGladiator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGladiator(AGladiator&&); \
	NO_API AGladiator(const AGladiator&); \
public:


#define New_folder___Copy_Source_RPG2_Gladiator_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGladiator(AGladiator&&); \
	NO_API AGladiator(const AGladiator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGladiator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGladiator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGladiator)


#define New_folder___Copy_Source_RPG2_Gladiator_h_18_PRIVATE_PROPERTY_OFFSET
#define New_folder___Copy_Source_RPG2_Gladiator_h_15_PROLOG
#define New_folder___Copy_Source_RPG2_Gladiator_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	New_folder___Copy_Source_RPG2_Gladiator_h_18_PRIVATE_PROPERTY_OFFSET \
	New_folder___Copy_Source_RPG2_Gladiator_h_18_RPC_WRAPPERS \
	New_folder___Copy_Source_RPG2_Gladiator_h_18_INCLASS \
	New_folder___Copy_Source_RPG2_Gladiator_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define New_folder___Copy_Source_RPG2_Gladiator_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	New_folder___Copy_Source_RPG2_Gladiator_h_18_PRIVATE_PROPERTY_OFFSET \
	New_folder___Copy_Source_RPG2_Gladiator_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	New_folder___Copy_Source_RPG2_Gladiator_h_18_INCLASS_NO_PURE_DECLS \
	New_folder___Copy_Source_RPG2_Gladiator_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RPG2_API UClass* StaticClass<class AGladiator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID New_folder___Copy_Source_RPG2_Gladiator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
