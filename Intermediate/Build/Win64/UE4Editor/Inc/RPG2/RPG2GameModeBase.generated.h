// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RPG2_RPG2GameModeBase_generated_h
#error "RPG2GameModeBase.generated.h already included, missing '#pragma once' in RPG2GameModeBase.h"
#endif
#define RPG2_RPG2GameModeBase_generated_h

#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_RPC_WRAPPERS
#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARPG2GameModeBase(); \
	friend struct Z_Construct_UClass_ARPG2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ARPG2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/RPG2"), NO_API) \
	DECLARE_SERIALIZER(ARPG2GameModeBase)


#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARPG2GameModeBase(); \
	friend struct Z_Construct_UClass_ARPG2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ARPG2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/RPG2"), NO_API) \
	DECLARE_SERIALIZER(ARPG2GameModeBase)


#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARPG2GameModeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARPG2GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARPG2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARPG2GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARPG2GameModeBase(ARPG2GameModeBase&&); \
	NO_API ARPG2GameModeBase(const ARPG2GameModeBase&); \
public:


#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARPG2GameModeBase(ARPG2GameModeBase&&); \
	NO_API ARPG2GameModeBase(const ARPG2GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARPG2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARPG2GameModeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARPG2GameModeBase)


#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_12_PROLOG
#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_RPC_WRAPPERS \
	New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_INCLASS \
	New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	New_folder___Copy_Source_RPG2_RPG2GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RPG2_API UClass* StaticClass<class ARPG2GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID New_folder___Copy_Source_RPG2_RPG2GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
