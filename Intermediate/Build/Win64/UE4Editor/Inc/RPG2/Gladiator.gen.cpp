// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "RPG2/Gladiator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGladiator() {}
// Cross Module References
	RPG2_API UClass* Z_Construct_UClass_AGladiator_NoRegister();
	RPG2_API UClass* Z_Construct_UClass_AGladiator();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_RPG2();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_GetIsStillAlive();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_MoveForward();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_MoveRight();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_OnAttack();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_OnChangeWeapon();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_OnGetHealthAmount();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_OnPostAttack();
	RPG2_API UFunction* Z_Construct_UFunction_AGladiator_OnSetPlayerController();
	RPG2_API UClass* Z_Construct_UClass_AGameDataTables_NoRegister();
	PAPER2D_API UClass* Z_Construct_UClass_UPaperSpriteComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
// End Cross Module References
	void AGladiator::StaticRegisterNativesAGladiator()
	{
		UClass* Class = AGladiator::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetIsStillAlive", &AGladiator::execGetIsStillAlive },
			{ "MoveForward", &AGladiator::execMoveForward },
			{ "MoveRight", &AGladiator::execMoveRight },
			{ "OnAttack", &AGladiator::execOnAttack },
			{ "OnChangeHealthByAmount", &AGladiator::execOnChangeHealthByAmount },
			{ "OnChangeWeapon", &AGladiator::execOnChangeWeapon },
			{ "OnGetHealthAmount", &AGladiator::execOnGetHealthAmount },
			{ "OnPostAttack", &AGladiator::execOnPostAttack },
			{ "OnSetPlayerController", &AGladiator::execOnSetPlayerController },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics
	{
		struct Gladiator_eventGetIsStillAlive_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Gladiator_eventGetIsStillAlive_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Gladiator_eventGetIsStillAlive_Parms), &Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//Getter method to return if the player is dead or alive\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Getter method to return if the player is dead or alive" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "GetIsStillAlive", nullptr, nullptr, sizeof(Gladiator_eventGetIsStillAlive_Parms), Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_GetIsStillAlive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_GetIsStillAlive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGladiator_MoveForward_Statics
	{
		struct Gladiator_eventMoveForward_Parms
		{
			float Value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AGladiator_MoveForward_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Gladiator_eventMoveForward_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGladiator_MoveForward_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGladiator_MoveForward_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_MoveForward_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Actions" },
		{ "Comment", "//Method that holds code responsible for player movement forward or backward, callable from Gladiator class blueprint instance\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Method that holds code responsible for player movement forward or backward, callable from Gladiator class blueprint instance" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_MoveForward_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "MoveForward", nullptr, nullptr, sizeof(Gladiator_eventMoveForward_Parms), Z_Construct_UFunction_AGladiator_MoveForward_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_MoveForward_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_MoveForward_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_MoveForward_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_MoveForward()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_MoveForward_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGladiator_MoveRight_Statics
	{
		struct Gladiator_eventMoveRight_Parms
		{
			float Value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AGladiator_MoveRight_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Gladiator_eventMoveRight_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGladiator_MoveRight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGladiator_MoveRight_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_MoveRight_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Actions" },
		{ "Comment", "//Responsible for player movement to the left and right\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Responsible for player movement to the left and right" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_MoveRight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "MoveRight", nullptr, nullptr, sizeof(Gladiator_eventMoveRight_Parms), Z_Construct_UFunction_AGladiator_MoveRight_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_MoveRight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_MoveRight_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_MoveRight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_MoveRight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_MoveRight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGladiator_OnAttack_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_OnAttack_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Actions" },
		{ "Comment", "//A method that is responsible for attacking\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "A method that is responsible for attacking" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_OnAttack_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "OnAttack", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_OnAttack_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnAttack_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_OnAttack()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_OnAttack_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics
	{
		struct Gladiator_eventOnChangeHealthByAmount_Parms
		{
			float usedAmount;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_usedAmount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::NewProp_usedAmount = { "usedAmount", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Gladiator_eventOnChangeHealthByAmount_Parms, usedAmount), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::NewProp_usedAmount,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//The attack effect on health\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "The attack effect on health" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "OnChangeHealthByAmount", nullptr, nullptr, sizeof(Gladiator_eventOnChangeHealthByAmount_Parms), Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGladiator_OnChangeWeapon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_OnChangeWeapon_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Actions" },
		{ "Comment", "//A method that is responsible for switching between weapons\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "A method that is responsible for switching between weapons" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_OnChangeWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "OnChangeWeapon", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_OnChangeWeapon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnChangeWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_OnChangeWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_OnChangeWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics
	{
		struct Gladiator_eventOnGetHealthAmount_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Gladiator_eventOnGetHealthAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//Getter function that returns TotalHealth value of player as float\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Getter function that returns TotalHealth value of player as float" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "OnGetHealthAmount", nullptr, nullptr, sizeof(Gladiator_eventOnGetHealthAmount_Parms), Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_OnGetHealthAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_OnGetHealthAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGladiator_OnPostAttack_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_OnPostAttack_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Actions" },
		{ "Comment", "//Method that holds procedurals after the player has attacked\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Method that holds procedurals after the player has attacked" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_OnPostAttack_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "OnPostAttack", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_OnPostAttack_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnPostAttack_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_OnPostAttack()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_OnPostAttack_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics
	{
		struct Gladiator_eventOnSetPlayerController_Parms
		{
			bool status;
		};
		static void NewProp_status_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::NewProp_status_SetBit(void* Obj)
	{
		((Gladiator_eventOnSetPlayerController_Parms*)Obj)->status = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Gladiator_eventOnSetPlayerController_Parms), &Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::NewProp_status_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::NewProp_status,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//Enable or disable inputs\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Enable or disable inputs" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGladiator, nullptr, "OnSetPlayerController", nullptr, nullptr, sizeof(Gladiator_eventOnSetPlayerController_Parms), Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGladiator_OnSetPlayerController()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGladiator_OnSetPlayerController_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AGladiator_NoRegister()
	{
		return AGladiator::StaticClass();
	}
	struct Z_Construct_UClass_AGladiator_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttackRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AttackRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TotalHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TablesInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TablesInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsControlable_MetaData[];
#endif
		static void NewProp_IsControlable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsControlable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_WeaponIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsAttacking_MetaData[];
#endif
		static void NewProp_IsAttacking_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsAttacking;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsStillAlive_MetaData[];
#endif
		static void NewProp_IsStillAlive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsStillAlive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_jumppingVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_jumppingVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseLookUpRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseLookUpRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectSprite_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EffectSprite;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGladiator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_RPG2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AGladiator_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AGladiator_GetIsStillAlive, "GetIsStillAlive" }, // 1893488391
		{ &Z_Construct_UFunction_AGladiator_MoveForward, "MoveForward" }, // 2726145752
		{ &Z_Construct_UFunction_AGladiator_MoveRight, "MoveRight" }, // 2375124170
		{ &Z_Construct_UFunction_AGladiator_OnAttack, "OnAttack" }, // 1928665919
		{ &Z_Construct_UFunction_AGladiator_OnChangeHealthByAmount, "OnChangeHealthByAmount" }, // 431287566
		{ &Z_Construct_UFunction_AGladiator_OnChangeWeapon, "OnChangeWeapon" }, // 3766909779
		{ &Z_Construct_UFunction_AGladiator_OnGetHealthAmount, "OnGetHealthAmount" }, // 1526314553
		{ &Z_Construct_UFunction_AGladiator_OnPostAttack, "OnPostAttack" }, // 592947021
		{ &Z_Construct_UFunction_AGladiator_OnSetPlayerController, "OnSetPlayerController" }, // 2615980381
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "//#include \"GameDataTables.h\"\n" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Gladiator.h" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "#include \"GameDataTables.h\"" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_AttackRange_MetaData[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//The range for the hero attack\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "The range for the hero attack" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_AttackRange = { "AttackRange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, AttackRange), METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_AttackRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_AttackRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_TotalHealth_MetaData[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//The health of the hero\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "The health of the hero" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_TotalHealth = { "TotalHealth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, TotalHealth), METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_TotalHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_TotalHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_TablesInstance_MetaData[] = {
		{ "Category", "Game DataTables" },
		{ "Comment", "//Datatable variable to hold active instance of game tables and load data\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Datatable variable to hold active instance of game tables and load data" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_TablesInstance = { "TablesInstance", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, TablesInstance), Z_Construct_UClass_AGameDataTables_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_TablesInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_TablesInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_IsControlable_MetaData[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//To be able to disable the player during cutscenes, menu, death... etc\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "To be able to disable the player during cutscenes, menu, death... etc" },
	};
#endif
	void Z_Construct_UClass_AGladiator_Statics::NewProp_IsControlable_SetBit(void* Obj)
	{
		((AGladiator*)Obj)->IsControlable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_IsControlable = { "IsControlable", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AGladiator), &Z_Construct_UClass_AGladiator_Statics::NewProp_IsControlable_SetBit, METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_IsControlable_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_IsControlable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_WeaponIndex_MetaData[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//The index of the current active weapon.\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "The index of the current active weapon." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_WeaponIndex = { "WeaponIndex", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, WeaponIndex), METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_WeaponIndex_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_WeaponIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_IsAttacking_MetaData[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//Is the player attacking right now?\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Is the player attacking right now?" },
	};
#endif
	void Z_Construct_UClass_AGladiator_Statics::NewProp_IsAttacking_SetBit(void* Obj)
	{
		((AGladiator*)Obj)->IsAttacking = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_IsAttacking = { "IsAttacking", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AGladiator), &Z_Construct_UClass_AGladiator_Statics::NewProp_IsAttacking_SetBit, METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_IsAttacking_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_IsAttacking_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_IsStillAlive_MetaData[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//Is the player dead or not\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Is the player dead or not" },
	};
#endif
	void Z_Construct_UClass_AGladiator_Statics::NewProp_IsStillAlive_SetBit(void* Obj)
	{
		((AGladiator*)Obj)->IsStillAlive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_IsStillAlive = { "IsStillAlive", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AGladiator), &Z_Construct_UClass_AGladiator_Statics::NewProp_IsStillAlive_SetBit, METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_IsStillAlive_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_IsStillAlive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_jumppingVelocity_MetaData[] = {
		{ "Category", "Player Attributes" },
		{ "Comment", "//Base Jump velocity\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Base Jump velocity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_jumppingVelocity = { "jumppingVelocity", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, jumppingVelocity), METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_jumppingVelocity_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_jumppingVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_BaseLookUpRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "//Base look up/down rate, in deg/sec. Other scaling may affect final rate.\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Base look up/down rate, in deg/sec. Other scaling may affect final rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_BaseLookUpRate = { "BaseLookUpRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, BaseLookUpRate), METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_BaseLookUpRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_BaseLookUpRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_BaseTurnRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "//Base turn rate in deg/sec. Other scaling may affect final turn rate.\n" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Base turn rate in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_BaseTurnRate = { "BaseTurnRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, BaseTurnRate), METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_BaseTurnRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_EffectSprite_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Effects" },
		{ "Comment", "//The sprite used to draw effect, better and more controllable than using the HUD or Textures\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "The sprite used to draw effect, better and more controllable than using the HUD or Textures" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_EffectSprite = { "EffectSprite", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, EffectSprite), Z_Construct_UClass_UPaperSpriteComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_EffectSprite_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_EffectSprite_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "//Follow camera\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_FollowCamera = { "FollowCamera", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_FollowCamera_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGladiator_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "//Camera boom positioning the camera behind the character\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Gladiator.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGladiator_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGladiator, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::NewProp_CameraBoom_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::NewProp_CameraBoom_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGladiator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_AttackRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_TotalHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_TablesInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_IsControlable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_WeaponIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_IsAttacking,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_IsStillAlive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_jumppingVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_BaseLookUpRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_BaseTurnRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_EffectSprite,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_FollowCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGladiator_Statics::NewProp_CameraBoom,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGladiator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGladiator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGladiator_Statics::ClassParams = {
		&AGladiator::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AGladiator_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AGladiator_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AGladiator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGladiator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGladiator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGladiator, 2029305251);
	template<> RPG2_API UClass* StaticClass<AGladiator>()
	{
		return AGladiator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGladiator(Z_Construct_UClass_AGladiator, &AGladiator::StaticClass, TEXT("/Script/RPG2"), TEXT("AGladiator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGladiator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
