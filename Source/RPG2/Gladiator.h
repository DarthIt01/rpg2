// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "PaperSpriteComponent.h"
#include "GameDataTables.h"
#include "GameFramework/Character.h"
#include "Gladiator.generated.h"

//#include "GameDataTables.h"

UCLASS(config = Game)
class RPG2_API AGladiator : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGladiator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Camera boom positioning the camera behind the character
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	//Follow camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	//The sprite used to draw effect, better and more controllable than using the HUD or Textures
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Effects, meta = (AllowPrivateAccess = "true"))
		class UPaperSpriteComponent* EffectSprite;
	
	//In order to make a plugin header to work (for example PaperSpriteComponent.h from Paper2D)
	//add Paper2D to *.Build.cs public dependency array
	//go to Project -> * Properties -> NMake -> Include Search Path -> <Edit...> and add the following:
	//C:/Program Files/Epic Games/UE_4.23/Engine/Plugins/2D/Paper2D/Intermediate/Build/Win64/UE4Editor/Inc/Paper2D
	//C:/Program Files/Epic Games/UE_4.23/Engine/Plugins/2D/Paper2D/Source
	//C:/Program Files/Epic Games/UE_4.23/Engine/Plugins/2D/Paper2D/Source/Paper2D/Classes
	//C:/Program Files/Epic Games/UE_4.23/Engine/Plugins/2D/Paper2D/Source/Paper2D/Public
	//C:/Program Files/Epic Games/UE_4.23/Engine/Plugins/2D/Paper2D/Source/Paper2D
	//in the editor File -> Refresh Visual Studio Project should be used always!!! after adding a new module or plugin 
	//recompile it

	//in the project folder right click *.uproject -> Generate Visual Studio project files should be used only!!! if missing source code



	//Base turn rate in deg/sec. Other scaling may affect final turn rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	//Base look up/down rate, in deg/sec. Other scaling may affect final rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	//Base Jump velocity
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Player Attributes")
		float jumppingVelocity;

	//Is the player dead or not
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Attributes")
		bool IsStillAlive;

	//Is the player attacking right now?
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Attributes")
		bool IsAttacking;

	//The index of the current active weapon.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Attributes")
		int32 WeaponIndex;

	//To be able to disable the player during cutscenes, menu, death... etc
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Attributes")
		bool IsControlable;

	//Datatable variable to hold active instance of game tables and load data
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Game DataTables")
		AGameDataTables* TablesInstance;

	//Getter method to return if the player is dead or alive
	UFUNCTION(BlueprintCallable, Category = "Player Attributes")
		bool GetIsStillAlive() const { return IsStillAlive; }

	//Enable or disable inputs
	UFUNCTION(BlueprintCallable, Category = "Player Attributes")
		void OnSetPlayerController(bool status);

	//The attack effect on health
	UFUNCTION(BlueprintCallable, Category = "Player Attributes")
		void OnChangeHealthByAmount(float usedAmount);

	//Getter function that returns TotalHealth value of player as float
	UFUNCTION(BlueprintCallable, Category = "Player Attributes")
		float OnGetHealthAmount() const { return TotalHealth; }

	//Method that holds procedurals after the player has attacked
	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void OnPostAttack();

	//Returns CameraBoom subobject or component variable
	FORCEINLINE	class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	//Returns FollowCamera subobject
	FORCEINLINE	class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	//protected:

		//Method that holds code responsible for player movement forward or backward, callable from Gladiator class blueprint instance
	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void MoveForward(float Value);

	//Responsible for player movement to the left and right
	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void MoveRight(float Value);

	//Method that is responsible for applying the jump action to the character based on the base character class
	//UFUNCTION(BlueprintCallable, Category = "Player Actions")
	void Jump() override;

	//Method responsible for stopping the jump, and resuming the idle/run animation
	//UFUNCTION(BlueprintCallable, Category = "Player Actions")
	void StopJumping() override;

	//A method that is responsible for attacking
	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void OnAttack();

	//A method that is responsible for switching between weapons
	UFUNCTION(BlueprintCallable, Category = "Player Actions")
		void OnChangeWeapon();

	//Method responsible for applying turns to the following camera, called via input to turn at a given rate.
	void TurnAtRate(float Rate);

	//Responsible for applying camera look-up rate, called via input to turn up/down at a given rate.
	void LookUpAtRate(float Rate);

	//The health of the hero
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Attributes")
		float TotalHealth;

	//The range for the hero attack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Attributes")
		float AttackRange;

protected:

	//APawn interface
	//Another override of a virtual function from the base class, called to set up and map the key inputs
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	//End of APawn interface
};
