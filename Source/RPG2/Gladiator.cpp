// Fill out your copyright notice in the Description page of Project Settings.


#include "Gladiator.h"
#include "RPG2.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "EngineUtils.h"
#include "Engine/Engine.h"
#include "Components/InputComponent.h"
#include "Misc/OutputDeviceNull.h"
#include "GameFramework/Controller.h"
//#include "PaperSpriteComponent.h"
//#include "GameData.h"



// Sets default values
AGladiator::AGladiator()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.0f, 96.0f);

	//Initializing hero's health
	TotalHealth = 100.0f;

	//Initializing hero's attack range
	AttackRange = 25.0f;

	//Initializing hero's jump velocity
	jumppingVelocity = 600.0f;

	//Setting hero's turn and look-up rates for input
	BaseTurnRate = 45.0f;
	BaseLookUpRate = 600.0f;

	//Don't rotate when the controller rotates, let that just affect the camera
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	//Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ... at this rotation rate
	GetCharacterMovement()->JumpZVelocity = jumppingVelocity;
	GetCharacterMovement()->AirControl = 0.2f;

	//Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	//Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera doesn't rotate relative to arm

	EffectSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("ClawEffect"));
	EffectSprite->SetupAttachment(CameraBoom);

	//Note that skeletal mesh and anim blueprint references on the Mesh component (inherited from Character)
	//are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	IsStillAlive = true;
	IsAttacking = false;
	WeaponIndex = 1;

	//by default inputs should be enabled, in case there is something needed to be tested
	OnSetPlayerController(true);

}

// Called when the game starts or when spawned
void AGladiator::BeginPlay()
{
	Super::BeginPlay();

	//Ask the data manager to get all the tables data at once and store them  
	/*AGameDataTables dataHolder;

	for (TActorIterator<AGameDataTables> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (ActorItr)
		{
			//print the instance name to screen
			GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Green, ActorItr->GetName());

			//Call the fetch to the tables, now we get all data stored. Why? simply because keep reading every time from the table itself is going to cost over your memory
			//but the safest method, is just to read all data at once, and then keep getting whatever needed values from the storage we have.
			TablesInstance = *ActorItr;
			TablesInstance->OnFetchAllTables();
		}
	}
	*/

}

// Called every frame
void AGladiator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGladiator::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Attack", IE_Released, this, &AGladiator::OnAttack);
	PlayerInputComponent->BindAction("ChangeWeapon", IE_Released, this, &AGladiator::OnChangeWeapon);

	PlayerInputComponent->BindAxis("MoveForward", this, &AGladiator::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGladiator::MoveRight);

	//Rotations bindings, "turnrate" has to be added to the editor in case of using analog stick control
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AGladiator::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AGladiator::LookUpAtRate);

}

void AGladiator::Jump()
{
	if (IsControlable && !IsAttacking)
	{
		bPressedJump = true;
		JumpKeyHoldTime = 0.0f;
	}
}

void AGladiator::StopJumping()
{
	if (IsControlable)
	{
		bPressedJump = false;
		JumpKeyHoldTime = 0.0f;
	}
}

void AGladiator::OnAttack()
{
	if (IsControlable)
	{
		IsAttacking = true;
	}
}

void AGladiator::OnPostAttack()
{
	IsAttacking = false;
}

void AGladiator::OnChangeWeapon()
{
	if (IsControlable)
	{
		if (WeaponIndex < TablesInstance->AllWeaponsData.Num())
		{
			WeaponIndex++;
		}
		else
		{
			WeaponIndex = 1;
		}
	}
}

void AGladiator::TurnAtRate(float Rate)
{
	if (IsControlable)
	{
		//Calculate delta for this frame from the rate information
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	}
}

void AGladiator::LookUpAtRate(float Rate)
{
	if (IsControlable)
	{
		//Calculate delta for this frame from the rate information
		AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
	}
}

void AGladiator::OnSetPlayerController(bool status)
{
	IsControlable = status;
}

void AGladiator::OnChangeHealthByAmount(float usedAmount)
{
	TotalHealth -= usedAmount;
	FOutputDeviceNull ar;
	this->CallFunctionByNameWithArguments(TEXT("ApplyGetDamageEffect"), ar, NULL, true);
}

void AGladiator::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && IsControlable && !IsAttacking)
	{
		//Find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		//Get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AGladiator::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && IsControlable && !IsAttacking)
	{
		//Find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		//Get right vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		//Add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

//How to create animations for our hero class:
//Open the skeleton with animations you want to copy to our hero  
//Open the hero skeleton, maybe it's named as Mannequin_Skeleton
//With skeletons open go to Set Up Rig in both of them on the Retarget Manager tab
//Select Rig: Select Humanoid Rig, in this case for both skeletons
//Map bones accordingly in Show Base or Show Advanced
//Right click the animation Retarget Anim Assets -> Duplicate Anim Assets and Retarget
//Select Skeleton of Hero -> Retarget

//To add a weapon or item to the Hero, open its blueprint BP
//Add Component -> Skeletal mesh, of the new item, then make it child of Hero's mesh
//Choose the socket added to the Hero's skeleton in the Animation preview