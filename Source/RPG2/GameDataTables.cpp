// Fill out your copyright notice in the Description page of Project Settings.


#include "GameDataTables.h"
#include "RPG2.h"

// Sets default values
AGameDataTables::AGameDataTables()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGameDataTables::BeginPlay()
{
	Super::BeginPlay();

	//Can call it at the beginning, but safer to call from the outside, from the player class
	OnFetchAllTables();

}

// Called every frame
void AGameDataTables::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGameDataTables::OnFetchAllTables()
{
	//Any will be okay, not necessary to be this cell name
	static const FString ContextString(TEXT("Name"));

	//Get all row names and store them temporarily, the point is to define the amount o
	TArray<FName> weaponsTableRowsNames = WeaponsTable->GetRowNames();

	//Usually the index start at 0, but a table have its first row indexed as 1, other
	for (int32 i = 1; i < weaponsTableRowsNames.Num() + 1; i++)
	{
		FString IndexString = FString::FromInt((int32)i);
		FName IndexName = FName(*IndexString);

		FWeaponStruct* aStructRow = WeaponsTable->FindRow<FWeaponStruct>(IndexName, ContextString, true);
		AllWeaponsData.Add(aStructRow);
	}

	//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, FString::FromInt(AllWeaponsData.Num()));

	//Just print to screen to check if all values are correct
	for (int32 i = 0; i < AllWeaponsData.Num(); i++)
	{
		FString message = TEXT(" Number: ") + FString::FromInt(i) + TEXT(" Name: ") + AllWeaponsData[i]->DisplayName + TEXT(" Icon: ") + AllWeaponsData[i]->Icon + TEXT(" Damage: ") + FString::FromInt(AllWeaponsData[i]->Damage) + TEXT(" Cooldown: ") + FString::FromInt(AllWeaponsData[i]->CooldownTime);
		//GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, message));
	}

	//Get all row names and store them temporary here, the point is to define the amount of rows, the best way yet
	TArray<FName> missionsTableRowsNames = MissionsTable->GetRowNames();

	//Usually used 0 as the start index, but a table have its first row indexed as 1, otherwise it will crash
	for (int32 i = 1; i < missionsTableRowsNames.Num() + 1; i++)
	{
		FString IndexString = FString::FromInt((int32)i);
		FName IndexName = FName(*IndexString);

		FMissionStruct* aStructRow = MissionsTable->FindRow<FMissionStruct>(IndexName, ContextString, true);
		AllMissionsData.Add(aStructRow);
	}

	//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, FString::FromInt(AllMissionsData.Num()));

	//Just print to screen to check if got all values correctly
	for (int32 i = 0; i < AllMissionsData.Num(); i++)
	{
		FString message = TEXT(" Number: ") + FString::FromInt(i) + TEXT(" Kills: ") + FString::FromInt(AllMissionsData[i]->Kill) + TEXT(" Collects") + FString::FromInt(AllMissionsData[i]->Collect);
		//GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Green, message));
	}

}

