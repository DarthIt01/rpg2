// Fill out your copyright notice in the Description page of Project Settings.


#include "RPG2GameModeBase.h"
#include "RPG2.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/World.h"

// Sets default values
ARPG2GameModeBase::ARPG2GameModeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/gladiator"));

	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

}

// Called when the game starts or when spawned
void ARPG2GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = GetWorld()->GetFirstPlayerController();

}